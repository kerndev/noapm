/*
* �������
* 446252221@qq.com
*/
#include "stdafx.h"
#include "MainBox.h"

static void MessageLoop(HWND hWnd)
{
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		if(!IsDialogMessage(hWnd,&msg))
		{
			DispatchMessage(&msg);
		}
	}
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR lpCmdLine, int nShowCmd)
{
	HWND hWnd;
	hWnd = CreateMainBox(hInst, NULL);
	if(!strstr(lpCmdLine,"--quiet"))
	{
		ShowWindow(hWnd,SW_SHOW);
	}
	MessageLoop(hWnd);
	return 0;
}
