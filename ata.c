/*
* SMART����ο�'CrystalDiskInfo'
* License : The MIT License
*/
#include "stdafx.h"
#include "ata.h"
#include <WinIoCtl.h>

#pragma pack(push,1)

typedef struct
{
	WORD		GeneralConfiguration;					//0
	WORD		LogicalCylinders;						//1	Obsolete
	WORD		SpecificConfiguration;					//2
	WORD		LogicalHeads;							//3 Obsolete
	WORD		Retired1[2];							//4-5
	WORD		LogicalSectors;							//6 Obsolete
	DWORD		ReservedForCompactFlash;				//7-8
	WORD		Retired2;								//9
	CHAR		SerialNumber[20];						//10-19
	WORD		Retired3;								//20
	WORD		BufferSize;								//21 Obsolete
	WORD		Obsolute4;								//22
	CHAR		FirmwareRev[8];							//23-26
	CHAR		Model[40];								//27-46
	WORD		MaxNumPerInterupt;						//47
	WORD		Reserved1;								//48
	WORD		Capabilities1;							//49
	WORD		Capabilities2;							//50
	DWORD		Obsolute5;								//51-52
	WORD		Field88and7064;							//53
	WORD		Obsolute6[5];							//54-58
	WORD		MultSectorStuff;						//59
	DWORD		TotalAddressableSectors;				//60-61
	WORD		Obsolute7;								//62
	WORD		MultiWordDma;							//63
	WORD		PioMode;								//64
	WORD		MinMultiwordDmaCycleTime;				//65
	WORD		RecommendedMultiwordDmaCycleTime;		//66
	WORD		MinPioCycleTimewoFlowCtrl;				//67
	WORD		MinPioCycleTimeWithFlowCtrl;			//68
	WORD		Reserved2[6];							//69-74
	WORD		QueueDepth;								//75
	WORD		SerialAtaCapabilities;					//76
	WORD		SerialAtaAdditionalCapabilities;		//77
	WORD		SerialAtaFeaturesSupported;				//78
	WORD		SerialAtaFeaturesEnabled;				//79
	WORD		MajorVersion;							//80
	WORD		MinorVersion;							//81
	WORD		CommandSetSupported1;					//82
	WORD		CommandSetSupported2;					//83
	WORD		CommandSetSupported3;					//84
	WORD		CommandSetEnabled1;						//85
	WORD		CommandSetEnabled2;						//86
	WORD		CommandSetDefault;						//87
	WORD		UltraDmaMode;							//88
	WORD		TimeReqForSecurityErase;				//89
	WORD		TimeReqForEnhancedSecure;				//90
	WORD		CurrentPowerManagement;					//91
	WORD		MasterPasswordRevision;					//92
	WORD		HardwareResetResult;					//93
	WORD		AcoustricManagement;					//94
	WORD		StreamMinRequestSize;					//95
	WORD		StreamingTimeDma;						//96
	WORD		StreamingAccessLatency;					//97
	DWORD		StreamingPerformance;					//98-99
	ULONGLONG	MaxUserLba;								//100-103
	WORD		StremingTimePio;						//104
	WORD		Reserved3;								//105
	WORD		SectorSize;								//106
	WORD		InterSeekDelay;							//107
	WORD		IeeeOui;								//108
	WORD		UniqueId3;								//109
	WORD		UniqueId2;								//110
	WORD		UniqueId1;								//111
	WORD		Reserved4[4];							//112-115
	WORD		Reserved5;								//116
	DWORD		WordsPerLogicalSector;					//117-118
	WORD		Reserved6[8];							//119-126
	WORD		RemovableMediaStatus;					//127
	WORD		SecurityStatus;							//128
	WORD		VendorSpecific[31];						//129-159
	WORD		CfaPowerMode1;							//160
	WORD		ReservedForCompactFlashAssociation[7];	//161-167
	WORD		DeviceNominalFormFactor;				//168
	WORD		DataSetManagement;						//169
	WORD		AdditionalProductIdentifier[4];			//170-173
	WORD		Reserved7[2];							//174-175
	CHAR		CurrentMediaSerialNo[60];				//176-205
	WORD		SctCommandTransport;					//206
	WORD		ReservedForCeAta1[2];					//207-208
	WORD		AlignmentOfLogicalBlocks;				//209
	DWORD		WriteReadVerifySectorCountMode3;		//210-211
	DWORD		WriteReadVerifySectorCountMode2;		//212-213
	WORD		NvCacheCapabilities;					//214
	DWORD		NvCacheSizeLogicalBlocks;				//215-216
	WORD		NominalMediaRotationRate;				//217
	WORD		Reserved8;								//218
	WORD		NvCacheOptions1;						//219
	WORD		NvCacheOptions2;						//220
	WORD		Reserved9;								//221
	WORD		TransportMajorVersionNumber;			//222
	WORD		TransportMinorVersionNumber;			//223
	WORD		ReservedForCeAta2[10];					//224-233
	WORD		MinimumBlocksPerDownloadMicrocode;		//234
	WORD		MaximumBlocksPerDownloadMicrocode;		//235
	WORD		Reserved10[19];							//236-254
	WORD		IntegrityWord;							//255
}ATA_IDENTIFY_DEVICE;

typedef struct
{
	WORD    Length;
	WORD    AtaFlags;
	BYTE    PathId;
	BYTE    TargetId;
	BYTE    Lun;
	BYTE    ReservedAsUchar;
	DWORD   DataTransferLength;
	DWORD   TimeOutValue;
	DWORD   ReservedAsUlong;
	//	DWORD   DataBufferOffset;
#ifdef _WIN64
	DWORD	padding;
#endif
	DWORD_PTR   DataBufferOffset;
	IDEREGS PreviousTaskFile;
	IDEREGS CurrentTaskFile;
} ATA_PASS_THROUGH_EX, *PCMD_ATA_PASS_THROUGH_EX;

typedef struct
{
	ATA_PASS_THROUGH_EX Apt;
	DWORD Filer;
	BYTE  Buf[512];
} ATA_PASS_THROUGH_EX_WITH_BUFFERS;
#pragma	pack(pop)

#define ATA_FLAGS_DRDY_REQUIRED	0x01
#define ATA_FLAGS_DATA_IN       0x02
#define ATA_FLAGS_DATA_OUT      0x04
#define ATA_FLAGS_48BIT_COMMAND 0x08

#define IOCTL_IDE_PASS_THROUGH  0x0004D028 // 2000 or later
#define IOCTL_ATA_PASS_THROUGH  0x0004D02C // XP SP2 and 2003 or later

//ATA�ַ�����2�ֽڽ���
static void SwapString(char* input, char* output, UINT len)
{
	UINT i;
	for(i=0;i<len;i+=2)
	{
		output[i] = input[i+1];
		output[i+1] = input[i];
	}
}

HANDLE GetDeviceHandle(BYTE index)
{
	char name[256];
	BYTE buf[512];
	DWORD dwReaded;
	HANDLE hDevice;
	sprintf(name,"\\\\.\\PhysicalDrive%d",index);
	hDevice = CreateFile(name,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,0,NULL);
	if(hDevice != INVALID_HANDLE_VALUE)
	{
		dwReaded = 0;
		SetFilePointer(hDevice, 0, NULL, FILE_BEGIN);
		ReadFile(hDevice, buf, 512, &dwReaded, NULL);
		if(dwReaded != 512)
		{
			return INVALID_HANDLE_VALUE;
		}
	}
	return hDevice;
}

BOOL SendAtaCommand(HANDLE hDevice, BYTE target, BYTE main, BYTE sub, BYTE param, PBYTE data, DWORD dataSize)
{
	BOOL	bRet;
	DWORD   size;
	DWORD	dwReturned;
	ATA_PASS_THROUGH_EX_WITH_BUFFERS ab;
	ZeroMemory(&ab, sizeof(ab));
	ab.Apt.Length = sizeof(ATA_PASS_THROUGH_EX);
	ab.Apt.TimeOutValue = 2;
	size = offsetof(ATA_PASS_THROUGH_EX_WITH_BUFFERS, Buf);
	ab.Apt.DataBufferOffset = size;

	if(dataSize > 0)
	{
		if(dataSize > sizeof(ab.Buf))
		{
			return FALSE;
		}
		ab.Apt.AtaFlags = ATA_FLAGS_DATA_IN;
		ab.Apt.DataTransferLength = dataSize;
		ab.Buf[0] = 0xCF; // magic number
		size += dataSize;
	}

	ab.Apt.CurrentTaskFile.bFeaturesReg = sub;
	ab.Apt.CurrentTaskFile.bSectorCountReg = param;
	ab.Apt.CurrentTaskFile.bDriveHeadReg = target;
	ab.Apt.CurrentTaskFile.bCommandReg = main;

	if(main == SMART_CMD)
	{
		ab.Apt.CurrentTaskFile.bCylLowReg		= SMART_CYL_LOW;
		ab.Apt.CurrentTaskFile.bCylHighReg		= SMART_CYL_HI;
		ab.Apt.CurrentTaskFile.bSectorCountReg  = 1;
		ab.Apt.CurrentTaskFile.bSectorNumberReg = 1;
	}

	bRet = DeviceIoControl(hDevice, IOCTL_ATA_PASS_THROUGH,&ab, size, &ab, size, &dwReturned, NULL);
	if(bRet && dataSize && data != NULL)
	{
		memcpy_s(data, dataSize, ab.Buf, dataSize);
	}
	return	bRet;
}

//target = 0xA0,0xB0
BOOL DisableSmartApm(HANDLE hDevice)
{
	return SendAtaCommand(hDevice, 0xA0, 0xEF, 0x85, 0, NULL, 0);
}

BOOL GetDiskIdentify(HANDLE hDevice, ATA_IDENTIFY_DEVICE* data)
{
	ZeroMemory(data,sizeof(ATA_IDENTIFY_DEVICE));
	return SendAtaCommand(hDevice, 0xA0, 0xEC, 0x00, 0x00, (PBYTE)data, sizeof(ATA_IDENTIFY_DEVICE));
}

BOOL IsApmSupported(ATA_IDENTIFY_DEVICE* data)
{
	return (data->CommandSetSupported2 & (1 << 3));
}

BOOL IsApmEnabled(ATA_IDENTIFY_DEVICE* data)
{
	return (data->CommandSetEnabled2 & (1 << 3));
}

void GetDiskModel(ATA_IDENTIFY_DEVICE* data, char* pModel)
{
	SwapString(data->Model, pModel, 40);
	pModel[39] = 0;
}

BOOL GetSmartInfo(HANDLE hDevice, ATA_SMART_INFO* data)
{
	ATA_IDENTIFY_DEVICE aid;
	if(!GetDiskIdentify(hDevice,&aid))
	{
		return FALSE;
	}
	data->IsApmSupported = IsApmSupported(&aid);
	data->IsApmEnabled   = IsApmEnabled(&aid);
	GetDiskModel(&aid,data->Model);
	return 1;
}
