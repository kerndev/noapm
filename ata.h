#pragma once

typedef struct 
{
	BOOL IsApmSupported;
	BOOL IsApmEnabled;
	char Model[40];
}ATA_SMART_INFO;

HANDLE GetDeviceHandle(BYTE index);
BOOL DisableSmartApm(HANDLE hDevice);
BOOL GetSmartInfo(HANDLE hDevice, ATA_SMART_INFO* data);
