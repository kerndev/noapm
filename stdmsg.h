/*
* 定义常用Windows消息的回调函数,名字与MFC相同
* 蒋晓岗<kerndev@foxmail.com>
* 2016.6.28 创建文件
*/
#pragma once

LRESULT OnActivate(HWND hWnd, UINT nState, BOOL bMinimized, HWND hDest);
LRESULT OnClose(HWND hWnd);
LRESULT OnCommand(HWND hWnd, UINT id, UINT code, HWND hFrom);
LRESULT OnContextMenu(HWND hWnd, HWND hDstWnd, int x, int y);
LRESULT OnCreate(HWND hWnd, LPCREATESTRUCT lpcs);
LRESULT OnCtlColor(HWND hWnd, HDC hdc, HWND hChild);
LRESULT OnDestroy(HWND hWnd);
LRESULT OnDeviceChange(HWND hWnd, UINT nEventType, LPARAM lParam);
LRESULT OnEnable(HWND hWnd, BOOL bEnable);
LRESULT OnEraseBkgnd(HWND hWnd, HDC hdc);
LRESULT OnGetMinMaxInfo(HWND hWnd, MINMAXINFO* lpMMI);
LRESULT OnHelp(HWND hWnd, HELPINFO* lpHelpInfo);
LRESULT OnHScroll(HWND hWnd, UINT nPos, UINT nSBCode, HWND hScroll);
LRESULT OnInitDialog(HWND hWnd, HWND hFocus, LPARAM lParam);
LRESULT OnKeyDown(HWND hWnd, WPARAM wKey, LPARAM lParam);
LRESULT OnKeyUp(HWND hWnd, WPARAM wKey, LPARAM lParam);
LRESULT OnKillFocus(HWND hWnd, HWND hNew);
LRESULT OnLButtonDblClk(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnLButtonDown(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnLButtonUp(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnMove(HWND hWnd, int x, int y);
LRESULT OnMouseMove(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnMouseWheel(HWND hWnd, UINT nFlags, short zDelta, int x, int y);
LRESULT OnNotify(HWND hWnd,int id,LPNMHDR pNMHDR);
LRESULT OnNcHitTest(HWND hWnd, int x, int y);
LRESULT OnPaint(HWND hWnd);
LRESULT OnRButtonDblClk(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnRButtonDown(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnRButtonUp(HWND hWnd, UINT nFlags, int x, int y);
LRESULT OnSetFocus(HWND hWnd, HWND hOld);
LRESULT OnShowWindow(HWND hWnd, BOOL bShow, UINT nStatus);
LRESULT OnSize(HWND hWnd, UINT nType, int w, int h);
LRESULT OnSysCommand(HWND hWnd, UINT nID, LPARAM lParam);
LRESULT OnSysKeyDown(HWND hWnd, UINT nKey, LPARAM lParam);
LRESULT OnSysKeyUp(HWND hWnd, UINT nKey, LPARAM lParam);
LRESULT OnTimer(HWND hWnd, WPARAM nID);
LRESULT OnVScroll(HWND hWnd,UINT nPos, UINT nSBCode, HWND hScrollBar);

//标准窗口处理函数
#define BEGIN_MESSAGE_MAP(name)	\
	static LRESULT WINAPI name(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)\
	{						\
		switch(msg)			\
		{					

#define END_MESSAGE_MAP()	\
		}					\
		return 0;			\
	}

//定义消息处理
#define ON_MESSAGE(msg, func)	\
	case msg:				return func(hWnd,wParam,lParam);

#define ON_WM_CLOSE()			\
	case WM_CLOSE:			return OnClose(hWnd);

#define ON_WM_COMMAND()			\
	case WM_COMMAND:		return OnCommand(hWnd, LOWORD(wParam), HIWORD(wParam), (HWND)lParam);

#define ON_WM_CONTEXTMENU()		\
	case WM_CONTEXTMENU:	return OnContextMenu(hWnd,(HWND)wParam,LOWORD(lParam),HIWORD(lParam));

#define ON_WM_CREATE()			\
	case WM_CREATE:			return OnCreate(hWnd,(LPCREATESTRUCT)wParam);

#define ON_WM_CTLCOLOR()		\
	case WM_CTLCOLOR:		return OnCtlColor(hWnd,(HDC)wParam,(HWND)lParam);

#define ON_WM_DESTROY()			\
	case WM_DESTROY:		return OnDestroy(hWnd);

#define ON_WM_DEVICECHANGE()	\
	case WM_DEVICECHANGE:	return OnDeviceChange(hWnd,wParam,lParam);

#define ON_WM_ERASEBKGND()		\
	case WM_ERASEBKGND:		return OnEraseBkgnd(hWnd,(HDC)wParam);

#define ON_WM_GETMINMAXINFO()	\
	case WM_GETMINMAXINFO:	return OnGetMinMaxInfo(hWnd,(LPMINMAXINFO)lParam);

#define ON_WM_INITDIALOG()		\
	case WM_INITDIALOG:		return OnInitDialog(hWnd,(HWND)wParam,lParam);

#define ON_WM_MOVE()			\
	case WM_MOVE:			return OnMove(hWnd,LOWORD(lParam),HIWORD(lParam));

#define ON_WM_NOTIFY()			\
	case WM_NOTIFY:			return OnNotify(hWnd,(int)wParam,(LPNMHDR)lParam);

#define ON_WM_PAINT()			\
	case WM_PAINT:			return OnPaint(hWnd);

#define ON_WM_SIZE()			\
	case WM_SIZE:			return OnSize(hWnd,(UINT)wParam,LOWORD(lParam),HIWORD(lParam));

#define ON_WM_SHOWWINDOW()		\
	case WM_SHOWWINDOW:		return OnShowWindow(hWnd,wParam,lParam);

#define ON_WM_NCHITTEST()		\
	case WM_NCHITTEST:		return OnNcHitTest(hWnd,LOWORD(lParam),HIWORD(lParam));

#define ON_WM_KEYDOWN()			\
	case WM_KEYDOWN:		return OnKeyDown(hWnd,wParam,lParam);

#define ON_WM_KEYUP()			\
	case WM_KEYUP:			return OnKeyUp(hWnd,wParam,lParam);

#define ON_WM_SYSKEYDOWN()		\
	case WM_SYSKEYDOWN:		return OnSysKeyDown(hWnd,wParam,lParam);

#define ON_WM_SYSKEYUP()		\
	case WM_SYSKEYUP:		return OnSysKeyUp(hWnd,wParam,lParam);

#define ON_WM_SYSCOMMAND()		\
	case WM_SYSCOMMAND:		return OnSysCommand(hWnd,wParam,lParam);

#define ON_WM_TIMER()			\
	case WM_TIMER:			return OnTimer(hWnd,wParam);

#define ON_WM_HELP()			\
	case WM_HELP:			return OnHelp(hWnd,(HELPINFO*)lParam);

#define ON_WM_HSCROLL()			\
	case WM_HSCROLL:		return OnHScroll(hWnd,LOWORD(wParam),HIWORD(wParam),(HWND)lParam);

#define ON_WM_VSCROLL()			\
	case WM_VSCROLL:		return OnVScroll(hWnd,LOWORD(wParam),HIWORD(wParam),(HWND)lParam);

#define ON_WM_MOUSEMOVE()		\
	case WM_MOUSEMOVE:		return OnMouseMove(hWnd, wParam, LOWORD(lParam),HIWORD(lParam));

#define ON_WM_LBUTTONDOWN()		\
	case WM_LBUTTONDOWN:	return OnLButtonDown(hWnd, wParam, LOWORD(lParam),HIWORD(lParam));

#define ON_WM_LBUTTONUP()		\
	case WM_LBUTTONUP:		return OnLButtonUp(hWnd, wParam, LOWORD(lParam),HIWORD(lParam));

#define ON_WM_LBUTTONDBLCLK()	\
	case WM_LBUTTONDBLCLK:	return OnLButtonDblClk(hWnd, (UINT)wParam, LOWORD(lParam),HIWORD(lParam));

#define ON_WM_RBUTTONDOWN()		\
	case WM_RBUTTONDOWN:	return OnRButtonDown(hWnd, wParam, LOWORD(lParam),HIWORD(lParam));

#define ON_WM_RBUTTONUP()		\
	case WM_RBUTTONUP:		return OnRButtonUp(hWnd, wParam, LOWORD(lParam),HIWORD(lParam));

#define ON_WM_RBUTTONDBLCLK()	\
	case WM_RBUTTONDBLCLK:	return OnRButtonDblClk(hWnd, wParam, LOWORD(lParam),HIWORD(lParam));

