#include "stdafx.h"
#include "stdmsg.h"
#include "resource.h"
#include "MainBox.h"
#include "ata.h"

static HANDLE hServiceThread;

#define WM_ATA_EVENT	WM_USER+100

static void SendText(HWND hWnd, BOOL bExit, LPCTSTR pText)
{
	SendMessage(hWnd,WM_ATA_EVENT,bExit,(LPARAM)pText);
}

static DWORD WINAPI ThreadFunc(LPVOID lParam)
{
	int i;
	HWND hWnd;
	HANDLE hDevice;
	ATA_SMART_INFO asi;
	char text[256];
	hWnd = (HWND)lParam;
	SendText(hWnd,0,"正在搜索硬盘...");
	for(i=0;i<32;i++)
	{
		hDevice = GetDeviceHandle(i);
		if(hDevice == INVALID_HANDLE_VALUE)
		{
			continue;
		}
		if(!GetSmartInfo(hDevice,&asi))
		{
			sprintf(text,"HD%d: 获取硬盘信息失败!",i);
			SendText(hWnd,0,text);
			CloseHandle(hDevice);
			continue;
		}
		sprintf(text,"HD%d: %s",i,asi.Model);
		SendText(hWnd,0,text);
		if(!asi.IsApmSupported)
		{
			sprintf(text,"     此硬盘不支持APM.");
			SendText(hWnd,0,text);
			CloseHandle(hDevice);
			continue;
		}
		if(!asi.IsApmEnabled)
		{
			sprintf(text,"     此硬盘APM未开启.");
			SendText(hWnd,0,text);
			CloseHandle(hDevice);
			continue;
		}
		if(!DisableSmartApm(hDevice))
		{
			sprintf(text,"     关闭硬盘APM失败!");
			SendText(hWnd,0,text);
			CloseHandle(hDevice);
			continue;
		}
		sprintf(text,"     关闭硬盘APM成功.");
		SendText(hWnd,0,text);
		CloseHandle(hDevice);
	}
	SendText(hWnd,0,"搜索完毕.");
	SendText(hWnd,1,"");
	return 0;
}


static LRESULT OnInitDialog(HWND hWnd, HWND hFocus, LPARAM lParam)
{
	DWORD pid;
	SetWindowText(hWnd,"硬盘APM禁用工具");
	hServiceThread = CreateThread(NULL,0,ThreadFunc,hWnd,0,&pid);
	if(hServiceThread==NULL)
	{
		MessageBox(hWnd, "启动线程失败", "初始化错误", MB_OK);
	}
	return 0;
}

static LRESULT OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
	return 0;
}

static LRESULT OnCommand(HWND hWnd, UINT id, UINT code, HWND hFrom)
{
	switch(id)
	{
	case IDOK:
	case IDCANCEL:
		DestroyWindow(hWnd);
		break;
	}
	return 0;
}

static LRESULT OnHelp(HWND hWnd, HELPINFO* lpHelpInfo)
{
	static BOOL bShow;
	char *help="1.请以管理员身份运行此程序\r\n2.--quiet 参数可以静默启动";
	if(bShow == 0)
	{
		bShow = 1;
		MessageBox(hWnd,help,"帮助",MB_OK);
		bShow = 0;
	}
	return 0;
}

static LRESULT OnEvent(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	HWND hListBox;
	hListBox = GetDlgItem(hWnd,IDC_LIST1);
	if(wParam==0)
	{
		SendMessage(hListBox,LB_ADDSTRING,0,lParam);
		return 0;
	}
	if(!IsWindowVisible(hWnd))
	{
		OnCommand(hWnd,IDOK,0,0);
	}
	return 0;
}


BEGIN_MESSAGE_MAP(MainBoxProc)
	ON_WM_HELP()
	ON_WM_INITDIALOG()
	ON_WM_COMMAND()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_ATA_EVENT,OnEvent)
END_MESSAGE_MAP()

HWND CreateMainBox(HINSTANCE hInst, HWND hParent)
{
	HWND hWnd;
	hWnd = CreateDialog(hInst,MAKEINTRESOURCE(IDD_MAIN), hParent, (DLGPROC)MainBoxProc);
	return hWnd;
}
